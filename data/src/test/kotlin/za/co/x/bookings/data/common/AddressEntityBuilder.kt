package za.co.x.bookings.data.common

import com.github.javafaker.Faker
import za.co.x.bookings.data.ref.CountryEntityBuilder

class AddressEntityBuilder {
    companion object {
        private val faker = Faker()
        fun randomAddress() =
                za.co.x.bookings.data.common.Address(null,
                        faker.address().streetAddress(),
                        faker.address().secondaryAddress(),
                        faker.address().buildingNumber(),
                        faker.address().cityName(),
                        faker.address().city(),
                        faker.address().cityName(),
                        CountryEntityBuilder.randomCountry()
                )
    }
}