package za.co.x.bookings.repositories.ref

import io.kotlintest.specs.StringSpec
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.test.context.ContextConfiguration
import za.co.x.bookings.data.business.BusinessEntityBuilder
import za.co.x.bookings.data.config.DataConfiguration
import za.co.x.bookings.data.ref.CategoryEntityBuilder
import za.co.x.bookings.data.ref.CountryEntityBuilder
import za.co.x.bookings.repositories.business.BusinessRepository


@RunWith(SpringRunner::class)
@DataJpaTest
@ContextConfiguration(classes= [DataConfiguration::class])
class BusinessRepositoryTest {

    @Autowired
    private lateinit var businessRepository: BusinessRepository

    @Autowired
    private lateinit var categoryRepository : CategoryRepository

    @Autowired
    private lateinit var countryRepository : CountryRepository
    @Test
    fun should_save_business() {
        val categoryEntity = categoryRepository.save(CategoryEntityBuilder.randomCategory())
        val countryEntity = countryRepository.save(CountryEntityBuilder.randomCountry())


        val businessEntity = BusinessEntityBuilder.randomBusiness()

        businessEntity.category = categoryEntity
        businessEntity.physicalAddress.country = countryEntity
        businessEntity.postalAddress.country = countryEntity

        val savedBusiness = businessRepository!!.save(businessEntity)
        savedBusiness.category
        assertThat(savedBusiness.businessId)
                .isNotNull()


    }
}