package za.co.x.bookings.data.business

import com.github.javafaker.Faker
import org.junit.Assert.*
import za.co.x.bookings.data.common.AddressEntityBuilder
import za.co.x.bookings.data.common.PhoneNumberEntityBuilder
import za.co.x.bookings.data.ref.CategoryEntityBuilder
import java.util.*

class BusinessEntityBuilder {
    companion object {
        private val faker = Faker()
        fun randomBusiness() = za.co.x.bookings.data.business.Business(
                faker.random().nextLong(),
                UUID.randomUUID(),
                CategoryEntityBuilder.randomCategory(),
                faker.name().name(),
                faker.lorem().paragraph(),
                faker.internet().emailAddress(),
                PhoneNumberEntityBuilder.randomPhoneNumber(),
                AddressEntityBuilder.randomAddress(),
                AddressEntityBuilder.randomAddress()
        )
    }
}