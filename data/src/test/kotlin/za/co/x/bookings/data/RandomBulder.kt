package za.co.x.bookings.data

import com.github.javafaker.Faker

class RandomBulder {
    companion object {
        val twoDigitRandom : String get() = Faker().numerify("##").toString()
        val threeDigitRandom : String get() = Faker().numerify("###").toString()
        val twoCharRandom : String get() = Faker().letterify("??")
        val threeCharRandom: String get() = Faker().letterify("???")
    }
}