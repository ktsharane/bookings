package za.co.x.bookings.repositories.ref

import za.co.x.bookings.data.ref.Country
import za.co.x.bookings.data.ref.CountryEntityBuilder
import java.util.*

class MyCountryRepository : CountryRepository {
    override fun deleteAll(entities: MutableIterable<Country>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteById(id: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <S : Country?> saveAll(entities: MutableIterable<S>?): MutableIterable<S> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAllById(ids: MutableIterable<String>?): MutableIterable<Country> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun existsById(id: String?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: String?): Optional<Country> {
        return Optional.ofNullable(CountryEntityBuilder.countryWithCode(id!!))
    }

    override fun <S : Country?> save(entity: S): S {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteAll() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }



    override fun count(): Long {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun delete(entity: Country?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): MutableIterable<Country> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}