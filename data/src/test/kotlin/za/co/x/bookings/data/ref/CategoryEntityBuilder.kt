package za.co.x.bookings.data.ref

import com.github.javafaker.Faker

class CategoryEntityBuilder {
    companion object {
        private val faker = Faker()
        fun randomCategory() =
                Category(faker.random().nextInt(10), faker.name().name(), faker.lorem().paragraph())

        fun categoryWithId(id: Int) =
                Category(id, faker.name().name(), faker.lorem().paragraph())

    }
}