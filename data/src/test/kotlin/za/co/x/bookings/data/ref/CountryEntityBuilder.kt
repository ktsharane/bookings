package za.co.x.bookings.data.ref

import com.github.javafaker.Faker
import org.junit.Assert.*
import za.co.x.bookings.data.RandomBulder

class CountryEntityBuilder {
    companion object {
        private val faker = Faker()
        fun randomCountry() = countryWithCode(RandomBulder.threeCharRandom)


        fun countryWithCode(countryCode : String) =
                Country(
                        countryCode,
                        faker.address().countryCode(),
                        faker.address().country(),
                        faker.address().country(),
                        faker.address().firstName(),
                        RandomBulder.twoCharRandom,
                        RandomBulder.threeCharRandom,
                        faker.letterify("????"),
                        RandomBulder.threeCharRandom,
                        RandomBulder.twoCharRandom,
                        RandomBulder.threeCharRandom,
                        RandomBulder.twoDigitRandom,
                        RandomBulder.threeDigitRandom,
                        RandomBulder.threeDigitRandom,
                        faker.letterify("???.PNG")
                )
    }
}