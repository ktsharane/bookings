package za.co.x.bookings.data.business

import com.github.javafaker.Faker
import za.co.x.bookings.data.common.AddressEntityBuilder
import za.co.x.bookings.data.common.PhoneNumberEntityBuilder

class BusinessLocationEntityBuilder {
    companion object {
        private val faker = Faker()
        fun randomBusinessLocation() =
                BusinessLocation(
                        faker.random().nextLong(),
                        BusinessEntityBuilder.randomBusiness(),
                        faker.name().name(),
                        faker.lorem().paragraph(),
                        faker.internet().emailAddress(),
                        PhoneNumberEntityBuilder.randomPhoneNumber(),
                        AddressEntityBuilder.randomAddress(),
                        AddressEntityBuilder.randomAddress())
    }
}