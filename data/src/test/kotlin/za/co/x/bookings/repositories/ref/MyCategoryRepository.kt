package za.co.x.bookings.repositories.ref

import za.co.x.bookings.data.ref.Category
import za.co.x.bookings.data.ref.CategoryEntityBuilder
import java.util.*

class MyCategoryRepository : CategoryRepository{
    override fun deleteAll(entities: MutableIterable<Category>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteById(id: Int?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun <S : Category?> saveAll(entities: MutableIterable<S>?): MutableIterable<S> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAllById(ids: MutableIterable<Int>?): MutableIterable<Category> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun existsById(id: Int?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findById(id: Int?): Optional<Category> {
        return Optional.ofNullable(CategoryEntityBuilder.categoryWithId(id!!)) //To change body of created functions use File | Settings | File Templates.
    }

    override fun delete(entity: Category?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }



    override fun <S : Category?> save(entity: S): S {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun deleteAll() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findAll(): MutableIterable<Category> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    /*override fun findOne(id: Int?): Category {
        return CategoryEntityBuilder.categoryWithId(id!!)
    }*/

    override fun count(): Long {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}