package za.co.x.bookings.data.ref

import com.github.javafaker.Faker

class CurrencyEntityBuilder {
    companion object {
        fun randomCurrency() = Currency("", "")
                .apply {
                    code = Faker().letterify("???")
                    name = Faker().name().fullName()
                    symbol = Faker().letterify("??")
                    symbolPosition = Faker().options().option("before", "after")
                }
    }
}