package za.co.x.bookings.data.common

import com.github.javafaker.Faker
import org.junit.Assert.*
import za.co.x.bookings.data.ref.CountryEntityBuilder

class PhoneNumberEntityBuilder {
    companion object {
        private val faker = Faker()
        fun randomPhoneNumber() =
                za.co.x.bookings.data.common.PhoneNumber(
                        country  =CountryEntityBuilder.randomCountry(),
                        phoneNumber = faker.phoneNumber().phoneNumber()
                )
    }
}