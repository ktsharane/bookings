package za.co.x.bookings.data.ref

import javax.persistence.Entity
import javax.persistence.Id

@Entity
class Currency(
        @Id var code: String ,
        var name: String ,
        var symbol: String = "",
        var symbolPosition: String = ""
)

