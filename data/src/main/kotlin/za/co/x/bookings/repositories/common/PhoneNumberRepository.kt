package za.co.x.bookings.repositories.common

import org.springframework.data.repository.CrudRepository
import za.co.x.bookings.data.common.PhoneNumber

interface PhoneNumberRepository : CrudRepository<PhoneNumber,Long>{
}