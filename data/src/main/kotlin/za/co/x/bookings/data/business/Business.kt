package za.co.x.bookings.data.business

import za.co.x.bookings.data.common.Address
import za.co.x.bookings.data.common.PhoneNumber
import za.co.x.bookings.data.ref.Category
import java.util.*
import javax.persistence.*


@Entity
class Business(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "business_business_id_seq")
        var businessId: Long? = null,
        var externalId: UUID? = UUID.randomUUID(),
        @ManyToOne(fetch = FetchType.EAGER)
        @JoinColumn(name = "category_id", referencedColumnName = "category_id")
        var category: Category? = null,
        var name: String,
        var description: String,
        var emailAddress: String,
        @OneToOne
        @JoinColumn(name = "contact_number")
        var phoneNumber: PhoneNumber,
        @OneToOne(cascade = [CascadeType.ALL])
        @JoinColumn(name = "physical_address")
        var physicalAddress: Address,
        @OneToOne(cascade = [CascadeType.ALL])
        @JoinColumn(name = "postal_address")
        var postalAddress: Address,
        @OneToMany(mappedBy = "business")
        var locations: MutableSet<BusinessLocation> = HashSet()
)