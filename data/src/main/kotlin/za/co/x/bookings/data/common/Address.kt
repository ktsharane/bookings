package za.co.x.bookings.data.common

import za.co.x.bookings.data.ref.Country
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
@Entity
class Address(@Id
              @GeneratedValue(strategy = GenerationType.SEQUENCE)
              var addressId: Long? = null,
              var line1 : String,
              var line2 : String,
              var line3 : String,
              var suburb : String,
              var city : String,
              var province : String,
              @ManyToOne var country : Country)