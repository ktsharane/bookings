package za.co.x.bookings.repositories.ref

import org.springframework.data.repository.CrudRepository
import za.co.x.bookings.data.ref.Country

interface CountryRepository : CrudRepository<Country, String> {

}