package za.co.x.bookings.data.config

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.transaction.annotation.EnableTransactionManagement

@Configuration
@EnableJpaRepositories(basePackages = arrayOf("za.co.x.bookings.repositories"))
@EntityScan(basePackages = arrayOf("za.co.x.bookings.data"))
@EnableTransactionManagement
class DataConfiguration {

}