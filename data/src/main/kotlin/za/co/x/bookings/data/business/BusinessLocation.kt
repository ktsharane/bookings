package za.co.x.bookings.data.business

import za.co.x.bookings.data.common.Address
import za.co.x.bookings.data.common.PhoneNumber
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne
@Entity
class BusinessLocation(
        @Id var businessLocationId: Long? = null,
        @ManyToOne
        @JoinColumn(name = "business_id")
        val business: Business,
        var name: String,
        var description: String,
        var emailAddress: String,
        @ManyToOne
        @JoinColumn(name = "contact_number")
        var phoneNumber: PhoneNumber,
        @ManyToOne
        @JoinColumn(name = "physical_address")
        var physicalAddress : Address,
        @ManyToOne
        @JoinColumn(name = "postal_address")
        var postalAddress : Address
        )