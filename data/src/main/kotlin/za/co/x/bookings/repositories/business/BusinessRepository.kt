package za.co.x.bookings.repositories.business

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import za.co.x.bookings.data.business.Business
@Repository
interface BusinessRepository : CrudRepository<Business, Long>