package za.co.x.bookings.data.common

import za.co.x.bookings.data.ref.Country
import javax.persistence.*

@Entity
class PhoneNumber(
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE)
        var phoneNumberId: Long? = null,
        @ManyToOne var country: Country,
        var phoneNumber: String
)