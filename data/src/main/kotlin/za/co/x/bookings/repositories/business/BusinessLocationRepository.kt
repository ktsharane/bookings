package za.co.x.bookings.repositories.business

import org.springframework.data.repository.CrudRepository
import za.co.x.bookings.data.business.BusinessLocation

interface BusinessLocationRepository : CrudRepository<BusinessLocation, Long>