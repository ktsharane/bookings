package za.co.x.bookings.data.ref

import za.co.x.bookings.data.business.Business
import java.util.HashSet
import javax.persistence.*

@Entity
class Category(
        @Id @GeneratedValue(strategy = GenerationType.AUTO ) @Column(name="category_id") var categoryId : Int,
        var name : String,
        var description: String,
        @OneToMany(fetch = FetchType.LAZY)
        var businesses: MutableSet<Business> = HashSet()
        )