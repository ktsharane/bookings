package za.co.x.bookings.repositories.ref

import org.springframework.data.repository.CrudRepository
import za.co.x.bookings.data.ref.Currency

interface CurrencyRepository : CrudRepository<Currency, String>