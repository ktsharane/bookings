package za.co.x.bookings.api

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import za.co.x.bookings.config.ServicesConfiguration

@Configuration
@ComponentScan
@Import(ServicesConfiguration::class)
open class APIConfiguration {

}