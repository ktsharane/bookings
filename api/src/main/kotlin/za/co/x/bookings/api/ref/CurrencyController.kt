package za.co.x.bookings.api.ref

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import za.co.x.bookings.ref.CurrencyService
import za.co.x.bookings.ref.Currency

@RestController
@RequestMapping("/api/v1/reference-lists/currency")
class CurrencyController (@Autowired private val currencyService: CurrencyService) {
    @RequestMapping(method = [RequestMethod.GET])
    fun findAll() : List<Currency> = currencyService.getAll()

}