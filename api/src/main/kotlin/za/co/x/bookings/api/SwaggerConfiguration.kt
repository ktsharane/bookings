package za.co.x.bookings.api


import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import org.springframework.context.annotation.Configuration
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.builders.PathSelectors.regex

@Configuration
@EnableSwagger2
open class SwaggerConfiguration {
    @Bean
    open fun productApi(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("za.co.x.bookings"))
                .build()
    }
}

/*
@SpringBootApplication
@EnableSwagger2
@ComponentScan( basePackages = arrayOf("za.co.x.bookings.api") )
open class Swagger2SpringBoot {

  @Bean
   open fun bookingsAPI () :Docket {
    return  Docket(DocumentationType.SWAGGER_2)
        .select()
          .apis(RequestHandlerSelectors.any())
          .paths(PathSelectors.any())
          .build()
        .pathMapping("/")
        .directModelSubstitute(LocalDate::class.java, String::class.java)
        .genericModelSubstitutes(ResponseEntity::class.java)
        .alternateTypeRules(
            newRule(typeResolver.resolve(DeferredResult::class.java,
                    typeResolver.resolve(ResponseEntity::class.java, WildcardType::class.java)),
                typeResolver.resolve(WildcardType::class.java)))
        .useDefaultResponseMessages(false)
        .globalResponseMessage(RequestMethod.GET,
            arrayListOf(ResponseMessageBuilder()
                .code(500)
                .message("500 message")
                .responseModel(ModelRef("Error"))
                .build()))
        .securitySchemes(arrayListOf(apiKey()))
        .securityContexts(arrayListOf(securityContext()))
        .enableUrlTemplating(true)
        .globalOperationParameters(
                arrayListOf(ParameterBuilder()
                .name("someGlobalParameter")
                .description("Description of someGlobalParameter")
                .modelRef( ModelRef("string"))
                .parameterType("query")
                .required(true)
                .build()))
        .tags( Tag("Pet Service", "All apis relating to pets"))
        //.additionalModels(typeResolver.resolve(AdditionalModel::class.java))

  }

  @Autowired
  private lateinit var typeResolver: TypeResolver;

  private fun apiKey() : ApiKey {
    return ApiKey("mykey", "api_key", "header");
  }

  private fun  securityContext(): SecurityContext {
    return SecurityContext.builder()
        .securityReferences(defaultAuth())
        .forPaths(PathSelectors.regex("/anyPath.*"))
        .build();
  }

   fun defaultAuth() :List<SecurityReference> {
    val authorizationScope = AuthorizationScope("global", "accessEverything")
       val authorizationScopes = arrayOf<AuthorizationScope>(authorizationScope);
    authorizationScopes[0] = authorizationScope
    return listOf(SecurityReference("mykey", authorizationScopes));
  }

  @Bean
   open fun security(): SecurityConfiguration {
    return  SecurityConfiguration(
        "test-app-client-id",
        "test-app-client-secret",
        "test-app-realm",
        "test-app",
        "apiKey",
        ApiKeyVehicle.HEADER,
        "api_key",
        "," */
/*scope separator*//*
);
  }

  @Bean
   open fun uiConfig(): UiConfiguration {
    return  UiConfiguration(
        "validatorUrl",// url
        "none",       // docExpansion          => none | list
        "alpha",      // apiSorter             => alpha
        "schema",     // defaultModelRendering => schema
        UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS,
        false,        // enableJsonEditor      => true | false
        true,         // showRequestHeaders    => true | false
        60000L);      // requestTimeout => in milliseconds, defaults to null (uses jquery xh timeout)
  }


}*/
