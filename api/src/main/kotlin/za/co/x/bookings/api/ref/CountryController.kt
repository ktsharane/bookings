package za.co.x.bookings.api.ref

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import za.co.x.bookings.ref.CountryService
import za.co.x.bookings.ref.Country

@RestController
@RequestMapping("/api/v1/reference-lists/country")
class CountryController(@Autowired private val countryService: CountryService) {
    @RequestMapping(method =[RequestMethod.GET])
    fun findAll() : List<Country> = countryService.getAll()

}