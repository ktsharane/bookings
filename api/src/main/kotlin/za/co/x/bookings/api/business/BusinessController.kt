package za.co.x.bookings.api.business

import org.springframework.stereotype.Controller
import za.co.x.bookings.business.Business
import za.co.x.bookings.business.BusinessService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

@RestController
@RequestMapping("/api/v1/business")
class BusinessController(@Autowired private val businessService: BusinessService){

    @PostMapping
    fun addBusiness(@RequestBody business: Business){
        ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString()

        val business =  businessService.addBusiness(business)
    }
}