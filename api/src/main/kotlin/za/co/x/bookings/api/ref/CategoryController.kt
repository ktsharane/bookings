package za.co.x.bookings.api.ref

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import za.co.x.bookings.ref.CategoryService

@RestController
@RequestMapping("/api/v1/reference-lists/category")
class CategoryController(private val categoryService: CategoryService) {
    @RequestMapping(method = [RequestMethod.GET])
    fun findAll() = categoryService.getAll()
}