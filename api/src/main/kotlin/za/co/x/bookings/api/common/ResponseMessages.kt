package za.co.x.bookings.api.common

class ResponseMessages(val displayMessage: String, val warning: List<String> = emptyList() , val errors: List<String> = emptyList()) {

}