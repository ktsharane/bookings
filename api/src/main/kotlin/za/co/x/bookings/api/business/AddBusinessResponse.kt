package za.co.x.bookings.api.business

import org.springframework.hateoas.Link
import org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo
import za.co.x.bookings.api.common.Response
import za.co.x.bookings.api.common.ResponseMessages
import za.co.x.bookings.business.Business

class AddBusinessResponse(response: ResponseMessages, newBusiness : Business) : Response(response) {
  val link = linkTo(BusinessController::class).slash(newBusiness.businessID).withSelfRel()
}