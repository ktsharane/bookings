package za.co.x.bookings.ref

class CurrencyMapper {
    companion object {

        fun mapFrom(currencyData: za.co.x.bookings.data.ref.Currency) =
                Currency(currencyData.code, currencyData.name, currencyData.symbol, currencyData.symbolPosition)

        fun mapFrom(currencies: Iterable<za.co.x.bookings.data.ref.Currency>) =
                currencies.map {
                    mapFrom(it)
                }
    }
}