package za.co.x.bookings.ref

import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import za.co.x.bookings.repositories.ref.CategoryRepository

@Service
class CategoryService(private val repository: CategoryRepository) {
    fun getAll() = CategoryMapper.mapFrom(repository.findAll())
}