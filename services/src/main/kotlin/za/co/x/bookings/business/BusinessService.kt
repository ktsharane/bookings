package za.co.x.bookings.business

import org.springframework.stereotype.Service
import za.co.x.bookings.repositories.business.BusinessRepository
import za.co.x.bookings.repositories.ref.CategoryRepository
import za.co.x.bookings.repositories.ref.CountryRepository
import java.util.*

@Service
class BusinessService(private val businessRepository: BusinessRepository, private val categoryRepository: CategoryRepository, private val countryRepository: CountryRepository) {
    fun addBusiness(business: Business): za.co.x.bookings.business.Business {
        val businessEntity = BusinessMapper.mapFrom(business, categoryRepository, countryRepository)
        val newBusiness = businessRepository.save(businessEntity)
        return BusinessMapper.mapFrom(newBusiness)

    }
}
