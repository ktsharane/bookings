package za.co.x.bookings.common

import za.co.x.bookings.repositories.ref.CountryRepository

class AddressMapper {
    companion object {
        fun mapFrom(address : za.co.x.bookings.data.common.Address) =
                Address(address.line1,
                        address.line2,
                        address.line3,
                        address.suburb,
                        address.city,
                        address.province,
                        address.country.countryCode)

        fun mapFrom(addresses : List<za.co.x.bookings.data.common.Address>) =
                addresses.map { mapFrom(it) }


        fun mapFrom(address: Address, countryRepository: CountryRepository) =
                za.co.x.bookings.data.common.Address(line1 = address.line1,
                        line2=address.line2,
                        line3=address.line3,
                        suburb = address.suburb,
                        city = address.city,
                        province = address.province,
                        country = countryRepository.findById(address.countryCode).get())

        fun mapFrom(addresses : List<Address>,countryRepository: CountryRepository) =
                addresses.map { mapFrom(it,countryRepository) }

    }
}