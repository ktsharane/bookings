package za.co.x.bookings.common

import za.co.x.bookings.repositories.ref.CountryRepository

class PhoneNumberMapper{
    companion object {
        fun mapFrom(phoneNumber: za.co.x.bookings.data.common.PhoneNumber) =
                PhoneNumber(phoneNumber.phoneNumber, phoneNumber.country.countryCode)

        fun  mapFrom(phoneNumbers: List<za.co.x.bookings.data.common.PhoneNumber>) =
                phoneNumbers.map { mapFrom(it) }

        fun mapFrom(phoneNumber: PhoneNumber, countryRepository: CountryRepository) =
            za.co.x.bookings.data.common.PhoneNumber(
                    phoneNumber = phoneNumber.phoneNumber,
                    country = countryRepository.findById(phoneNumber.countryCode).get())

        fun  mapFrom(phoneNumbers: List<PhoneNumber>,countryRepository: CountryRepository) =
                phoneNumbers.map { mapFrom(it,countryRepository) }

    }
}