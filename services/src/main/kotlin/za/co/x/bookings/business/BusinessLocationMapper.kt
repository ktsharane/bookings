package za.co.x.bookings.business

import za.co.x.bookings.common.AddressMapper
import za.co.x.bookings.common.PhoneNumberMapper
import za.co.x.bookings.data.business.Business
import za.co.x.bookings.repositories.ref.CountryRepository


class BusinessLocationMapper {
    companion object {
        fun mapFrom(businessLocation: BusinessLocation, business: Business, countryRepository: CountryRepository) =
                za.co.x.bookings.data.business.BusinessLocation(
                        business = business,
                        name = businessLocation.name,
                        description = businessLocation.description,
                        emailAddress = businessLocation.emailAddress,
                        phoneNumber = PhoneNumberMapper.mapFrom(businessLocation.phoneNumber, countryRepository),
                        postalAddress = AddressMapper.mapFrom(businessLocation.postalAddress, countryRepository),
                        physicalAddress = AddressMapper.mapFrom(businessLocation.physicalAddress, countryRepository)
                )

        fun mapFrom(businessLocation: za.co.x.bookings.data.business.BusinessLocation) =
                BusinessLocation(
                        name = businessLocation.name,
                        description = businessLocation.description,
                        emailAddress = businessLocation.emailAddress,
                        phoneNumber = PhoneNumberMapper.mapFrom(businessLocation.phoneNumber),
                        physicalAddress = AddressMapper.mapFrom(businessLocation.physicalAddress),
                        postalAddress = AddressMapper.mapFrom(businessLocation.postalAddress)
                )

        fun mapFrom(businessLocations: Iterable<za.co.x.bookings.data.business.BusinessLocation>) =
                businessLocations.map {
                    mapFrom(it)
                }

    }
}
