package za.co.x.bookings.ref


class  CategoryMapper {
    companion object {
        fun mapFrom(categoryData: za.co.x.bookings.data.ref.Category) =
                Category(categoryData.categoryId, categoryData.name, categoryData.description)

        fun mapFrom(categories: Iterable<za.co.x.bookings.data.ref.Category>) =
                categories.map {
                    mapFrom(it)
                }
    }
}