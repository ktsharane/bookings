package za.co.x.bookings.common

class Address(val line1 : String,
              val line2 : String,
              val line3 : String,
              val suburb : String,
              val city : String,
              val province: String,
              val countryCode : String)