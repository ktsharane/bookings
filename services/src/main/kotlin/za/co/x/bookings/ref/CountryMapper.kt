package za.co.x.bookings.ref

class CountryMapper {
    companion object {
        fun mapFrom(countryData: za.co.x.bookings.data.ref.Country) =
                Country(countryData.countryCode,
                        countryData.capital,
                        countryData.fullName,
                        countryData.name,
                        countryData.citizenship,
                        countryData.currency,
                        countryData.currencyCode,
                        countryData.currencySubUnit,
                        countryData.currencySymbol,
                        countryData.iso_3166_2,
                        countryData.iso_3166_3,
                        countryData.regionCode,
                        countryData.subRegionCode,
                        countryData.callingCode,
                        countryData.flag)

        fun mapFrom(countries: Iterable<za.co.x.bookings.data.ref.Country>) =
                countries.map {
                    mapFrom(it)
                }
    }
}