package za.co.x.bookings.ref

import org.springframework.stereotype.Component
import org.springframework.stereotype.Service
import za.co.x.bookings.repositories.ref.CountryRepository

@Service
class CountryService(private val repository: CountryRepository) {
    fun getAll() = CountryMapper.mapFrom(repository.findAll())

}