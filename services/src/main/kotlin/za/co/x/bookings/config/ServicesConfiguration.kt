package za.co.x.bookings.config

import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import za.co.x.bookings.data.config.DataConfiguration

@Configuration
@ComponentScan(basePackages = arrayOf("za.co.x.bookings"))
@Import(DataConfiguration::class)
open class ServicesConfiguration {

}