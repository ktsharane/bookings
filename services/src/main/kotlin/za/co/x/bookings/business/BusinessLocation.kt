package za.co.x.bookings.business

import za.co.x.bookings.common.Address
import za.co.x.bookings.common.PhoneNumber


class BusinessLocation(
        var name: String,
        var description: String,
        var emailAddress: String,
        var phoneNumber: PhoneNumber,
        var physicalAddress : Address,
        var postalAddress : Address
        )