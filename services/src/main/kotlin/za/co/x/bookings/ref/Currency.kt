package za.co.x.bookings.ref

class Currency(
        var code: String = "",
        var name: String = "",
        var symbol: String = "",
        var symbolPosition: String = ""
)

enum class SymbolPosition(name: String) {
    BEFORE("before"),
    AFTER("after")
}