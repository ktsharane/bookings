package za.co.x.bookings.common

import za.co.x.bookings.repositories.common.PhoneNumberRepository
import za.co.x.bookings.repositories.ref.CountryRepository

class PhoneNumberService(private val phoneNumberRepository: PhoneNumberRepository,private val countryRepository: CountryRepository) {
    fun addPhoneNumber(phoneNumber: PhoneNumber): za.co.x.bookings.data.common.PhoneNumber{
        return phoneNumberRepository.save(PhoneNumberMapper.mapFrom(phoneNumber,countryRepository))
    }
}