package za.co.x.bookings.ref

import org.springframework.stereotype.Component
import za.co.x.bookings.repositories.ref.CurrencyRepository


@Component
class CurrencyService(private val repository: CurrencyRepository) {

    fun getAll() = CurrencyMapper.mapFrom(repository.findAll())

}