package za.co.x.bookings.ref

import javax.persistence.Entity
import javax.persistence.Id

class  Country (@Id var countryCode: String,
                var capital: String,
                var fullName: String,
                var name: String ="",
                var citizenship: String ="",
                var currency: String ="",
                var currencyCode: String="",
                var currencySubunit: String="",
                var currencySymbol: String="",
                var iso_3166_2: String,
                var iso_3166_3: String,
                var regionCode: String,
                var subRegionCode: String,
                var callingCode: String="",
                var flag: String)