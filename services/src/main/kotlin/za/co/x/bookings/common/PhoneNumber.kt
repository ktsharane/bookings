package za.co.x.bookings.common

class PhoneNumber(val phoneNumber: String,
                  val countryCode: String)