package za.co.x.bookings.common

import za.co.x.bookings.repositories.common.AddressRepository
import za.co.x.bookings.repositories.ref.CountryRepository

class AddressService(private val repository: AddressRepository, private val countryRepository: CountryRepository) {
    fun createAddress(address: Address) :  za.co.x.bookings.data.common.Address{
        return repository.save(AddressMapper.mapFrom(address,countryRepository))
    }
}