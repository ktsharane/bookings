package za.co.x.bookings.business

import za.co.x.bookings.common.AddressMapper
import za.co.x.bookings.common.PhoneNumberMapper
import za.co.x.bookings.repositories.ref.CategoryRepository
import za.co.x.bookings.repositories.ref.CountryRepository

class BusinessMapper {
    companion object {
        fun mapFrom(business : Business, categoryRepository: CategoryRepository, countryRepository: CountryRepository): za.co.x.bookings.data.business.Business {
            val businessEntity = za.co.x.bookings.data.business.Business(
                    externalId = business.businessID,
                    category = categoryRepository.findById(business.category).get(),
                    name = business.name,
                    description = business.description,
                    emailAddress = business.emailAddress,
                    phoneNumber = PhoneNumberMapper.mapFrom(business.phoneNumber, countryRepository),
                    physicalAddress = AddressMapper.mapFrom(business.physicalAddress, countryRepository),
                    postalAddress = AddressMapper.mapFrom(business.postalAddress, countryRepository))

            if(business.locations != null && business.locations!!.isNotEmpty()){
                val locations = business.locations!!
                locations.forEach {
                    val locationEntity = BusinessLocationMapper.mapFrom(it,businessEntity,countryRepository)
                    businessEntity.locations.add(locationEntity)
                }
            }
            return businessEntity
        }

        fun mapFrom(business: za.co.x.bookings.data.business.Business) =
            Business(
                business.externalId,
                    business.category?.categoryId,
                    business.name,
                    business.description,
                    business.emailAddress,
                    PhoneNumberMapper.mapFrom(business.phoneNumber),
                    AddressMapper.mapFrom(business.physicalAddress),
                    AddressMapper.mapFrom(business.postalAddress),
                    BusinessLocationMapper.mapFrom(business.locations)

            )

    }
}