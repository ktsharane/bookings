package za.co.x.bookings.common

import com.github.javafaker.Faker
import za.co.x.bookings.ref.CountryBuilder

class PhoneNumberBuilder {
    companion object {
        private val faker = Faker()
        fun randomPhoneNumber() =
                PhoneNumber(faker.phoneNumber().cellPhone(), CountryBuilder.randomCountry().countryCode)


    }
}