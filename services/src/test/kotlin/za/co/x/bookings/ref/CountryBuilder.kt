package za.co.x.bookings.ref

import com.github.javafaker.Faker

class CountryBuilder {
    companion object {
        private val faker = Faker()
        fun randomCountry() = countryWithCode(RandomBuilder.threeCharRandom)


        private fun countryWithCode(countryCode: String) =
                Country(
                        countryCode,
                        faker.address().countryCode(),
                        faker.address().country(),
                        faker.address().country(),
                        faker.address().firstName(),
                        RandomBuilder.twoCharRandom,
                        RandomBuilder.threeCharRandom,
                        faker.letterify("????"),
                        RandomBuilder.threeCharRandom,
                        RandomBuilder.twoCharRandom,
                        RandomBuilder.threeCharRandom,
                        RandomBuilder.twoDigitRandom,
                        RandomBuilder.threeDigitRandom,
                        RandomBuilder.threeDigitRandom,
                        faker.letterify("???.PNG")
                )
    }
}


