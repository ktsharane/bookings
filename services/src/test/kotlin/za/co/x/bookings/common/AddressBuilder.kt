package za.co.x.bookings.common

import com.github.javafaker.Faker
import za.co.x.bookings.ref.CountryBuilder

class AddressBuilder {
    companion object {
        private val faker = Faker()
        fun randomAddress() =
                Address(
                        faker.address().buildingNumber(),
                        faker.address().streetAddressNumber(),
                        faker.address().streetName(),
                        faker.address().city(),
                        faker.address().cityName(),
                        faker.address().state(),
                        CountryBuilder.randomCountry().countryCode
                )


    }
}