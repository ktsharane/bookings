package za.co.x.bookings.ref

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.junit.experimental.categories.Category
import za.co.x.bookings.UnitTest
import za.co.x.bookings.data.ref.CategoryEntityBuilder

@Category(UnitTest::class)
class CategoryMapperTest : StringSpec() {
    init {
        "should map category data"{
            val categoryData = CategoryEntityBuilder.randomCategory()
            val mappedCategory = CategoryMapper.mapFrom(categoryData)

            mappedCategory.categoryId shouldBe categoryData.categoryId
            mappedCategory.name shouldBe  categoryData.name
            mappedCategory.description shouldBe  categoryData.description
        }

        "should map multiple categories"{
            val mappedCategories = CategoryMapper.mapFrom(listOf(CategoryEntityBuilder.randomCategory(), CategoryEntityBuilder.randomCategory(), CategoryEntityBuilder.randomCategory()))
            mappedCategories.size shouldBe 3
        }
    }
}