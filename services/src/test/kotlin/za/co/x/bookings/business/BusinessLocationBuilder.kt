package za.co.x.bookings.business

import com.github.javafaker.Faker
import za.co.x.bookings.common.AddressBuilder
import za.co.x.bookings.common.PhoneNumberBuilder

class BusinessLocationBuilder {
    companion object {
        private val faker = Faker()
        fun randomBusinessLocation() =
                BusinessLocation(faker.name().name(),
                        faker.lorem().paragraph(),
                        faker.internet().emailAddress(),
                        PhoneNumberBuilder.randomPhoneNumber(),
                        AddressBuilder.randomAddress(),
                        AddressBuilder.randomAddress())
    }
}