package za.co.x.bookings.ref


import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.junit.experimental.categories.Category
import za.co.x.bookings.UnitTest
import za.co.x.bookings.data.ref.CurrencyEntityBuilder

@Category(UnitTest::class)
class CurrencyMapperTestTest : StringSpec() {
    init {
        "should map currency data"{
            val currencyData = CurrencyEntityBuilder.randomCurrency()
            val mappedCurrency = CurrencyMapper.mapFrom(currencyData)

            mappedCurrency.code shouldBe  currencyData.code
            mappedCurrency.name shouldBe currencyData.name
            mappedCurrency.symbol shouldBe currencyData.symbol
            mappedCurrency.symbolPosition shouldBe  currencyData.symbolPosition
        }

        "should map multiple currencies"{
            CurrencyMapper.mapFrom(listOf(CurrencyEntityBuilder.randomCurrency(), CurrencyEntityBuilder.randomCurrency())).size shouldBe 2
        }
    }
}