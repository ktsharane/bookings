package za.co.x.bookings.ref

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.junit.experimental.categories.Category
import za.co.x.bookings.UnitTest
import za.co.x.bookings.data.ref.CountryEntityBuilder

@Category(UnitTest::class)
class CountryMapperTest : StringSpec() {
    init {
        "should map country data" {
            val countryEntity = CountryEntityBuilder.randomCountry()
            val mappedCountry = CountryMapper.mapFrom(countryEntity)

            mappedCountry.countryCode shouldBe countryEntity.countryCode
            mappedCountry.capital  shouldBe  countryEntity.capital
            mappedCountry.fullName  shouldBe  countryEntity.fullName
            mappedCountry.citizenship  shouldBe  countryEntity.citizenship
            mappedCountry.currency  shouldBe  countryEntity.currency
            mappedCountry.currencyCode  shouldBe  countryEntity.currencyCode
            mappedCountry.currencySubunit  shouldBe  countryEntity.currencySubUnit
            mappedCountry.currencySymbol  shouldBe  countryEntity.currencySymbol
            mappedCountry.iso_3166_2  shouldBe  countryEntity.iso_3166_2
            mappedCountry.iso_3166_3  shouldBe  countryEntity.iso_3166_3
            mappedCountry.regionCode  shouldBe  countryEntity.regionCode
            mappedCountry.subRegionCode  shouldBe  countryEntity.subRegionCode
            mappedCountry.callingCode  shouldBe  countryEntity.callingCode
            mappedCountry.flag  shouldBe  countryEntity.flag
        }

        "should map multiple countries"{
            CountryMapper.mapFrom(listOf(CountryEntityBuilder.randomCountry(), CountryEntityBuilder.randomCountry())).size shouldBe  2
        }
    }
}

