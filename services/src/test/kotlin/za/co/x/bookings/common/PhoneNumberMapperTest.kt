package za.co.x.bookings.common


import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.junit.experimental.categories.Category
import za.co.x.bookings.UnitTest
import za.co.x.bookings.data.common.PhoneNumberEntityBuilder

import za.co.x.bookings.repositories.ref.MyCountryRepository

@Category(UnitTest::class)
class  PhoneNumberMapperTest : StringSpec(){
    init {
        "should map phone number data"{
            val phoneNumberEntity = PhoneNumberEntityBuilder.randomPhoneNumber();
            val mappedPhoneNumber = PhoneNumberMapper.mapFrom(phoneNumberEntity)

            mappedPhoneNumber.phoneNumber shouldBe  phoneNumberEntity.phoneNumber
            mappedPhoneNumber.countryCode shouldBe phoneNumberEntity.country.countryCode
        }

        "should map multiple phone numbers"{
            val mappedPhoneNumbers = PhoneNumberMapper.mapFrom(listOf(PhoneNumberEntityBuilder.randomPhoneNumber(), PhoneNumberEntityBuilder.randomPhoneNumber()))
            mappedPhoneNumbers.size shouldBe 2
        }

        "should map phone number to entity"{
            val phoneNumber = PhoneNumberBuilder.randomPhoneNumber();
            val mappedPhoneNumber = PhoneNumberMapper.mapFrom(phoneNumber, MyCountryRepository())

            mappedPhoneNumber.phoneNumber shouldBe  phoneNumber.phoneNumber
            mappedPhoneNumber.country.countryCode shouldBe phoneNumber.countryCode
        }

        "should map multiple phone number entities"{
            val mappedPhoneNumbers = PhoneNumberMapper.mapFrom(listOf(PhoneNumberBuilder.randomPhoneNumber(), PhoneNumberBuilder.randomPhoneNumber()), MyCountryRepository())
            mappedPhoneNumbers.size shouldBe 2
        }
    }
}