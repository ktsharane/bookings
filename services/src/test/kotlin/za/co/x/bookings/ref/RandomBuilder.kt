package za.co.x.bookings.ref

import com.github.javafaker.Faker

class RandomBuilder {
    companion object {
        val twoDigitRandom : String get() = Faker().numerify("##").toString()
        val threeDigitRandom : String get() = Faker().numerify("###").toString()
        val twoCharRandom : String get() = Faker().letterify("??")
        val threeCharRandom: String get() = Faker().letterify("???")
    }
}