package za.co.x.bookings.business


import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.junit.experimental.categories.Category
import za.co.x.bookings.UnitTest
import za.co.x.bookings.data.business.BusinessEntityBuilder
import za.co.x.bookings.data.business.BusinessLocationEntityBuilder
import za.co.x.bookings.repositories.ref.MyCountryRepository

@Category(UnitTest::class)
class BusinessLocationMapperTest : StringSpec() {
    init {
        "should map business location" {
            val businessLocation = BusinessLocationBuilder.randomBusinessLocation()
            val mappedBusinessLocation = BusinessLocationMapper.mapFrom(businessLocation,
                    BusinessEntityBuilder.randomBusiness(), MyCountryRepository())

            mappedBusinessLocation.description shouldBe  businessLocation.description
            mappedBusinessLocation.name shouldBe  businessLocation.name
            mappedBusinessLocation.phoneNumber.phoneNumber shouldBe  businessLocation.phoneNumber.phoneNumber
            mappedBusinessLocation.phoneNumber.country.countryCode shouldBe  businessLocation.phoneNumber.countryCode
            mappedBusinessLocation.emailAddress shouldBe  businessLocation.emailAddress
            mappedBusinessLocation.physicalAddress.line1 shouldBe  businessLocation.physicalAddress.line1
            mappedBusinessLocation.physicalAddress.line2 shouldBe  businessLocation.physicalAddress.line2
            mappedBusinessLocation.physicalAddress.line3 shouldBe  businessLocation.physicalAddress.line3
            mappedBusinessLocation.physicalAddress.suburb shouldBe  businessLocation.physicalAddress.suburb
            mappedBusinessLocation.physicalAddress.city shouldBe  businessLocation.physicalAddress.city
            mappedBusinessLocation.physicalAddress.province shouldBe  businessLocation.physicalAddress.province
            mappedBusinessLocation.physicalAddress.country.countryCode shouldBe  businessLocation.physicalAddress.countryCode
            mappedBusinessLocation.postalAddress.line1 shouldBe  businessLocation.postalAddress.line1
            mappedBusinessLocation.postalAddress.line2 shouldBe  businessLocation.postalAddress.line2
            mappedBusinessLocation.postalAddress.line3 shouldBe  businessLocation.postalAddress.line3
            mappedBusinessLocation.postalAddress.suburb shouldBe  businessLocation.postalAddress.suburb
            mappedBusinessLocation.postalAddress.city shouldBe  businessLocation.postalAddress.city
            mappedBusinessLocation.postalAddress.province shouldBe  businessLocation.postalAddress.province
            mappedBusinessLocation.postalAddress.country.countryCode shouldBe  businessLocation.postalAddress.countryCode

        }

        "should map business location Entity" {
            val businessLocation = BusinessLocationEntityBuilder.randomBusinessLocation()
            val mappedBusinessLocation = BusinessLocationMapper.mapFrom(businessLocation)

            mappedBusinessLocation.description shouldBe  businessLocation.description
            mappedBusinessLocation.name shouldBe  businessLocation.name
            mappedBusinessLocation.phoneNumber.phoneNumber shouldBe  businessLocation.phoneNumber.phoneNumber
            mappedBusinessLocation.phoneNumber.countryCode shouldBe  businessLocation.phoneNumber.country.countryCode
            mappedBusinessLocation.emailAddress shouldBe  businessLocation.emailAddress
            mappedBusinessLocation.physicalAddress.line1 shouldBe  businessLocation.physicalAddress.line1
            mappedBusinessLocation.physicalAddress.line2 shouldBe  businessLocation.physicalAddress.line2
            mappedBusinessLocation.physicalAddress.line3 shouldBe  businessLocation.physicalAddress.line3
            mappedBusinessLocation.physicalAddress.suburb shouldBe  businessLocation.physicalAddress.suburb
            mappedBusinessLocation.physicalAddress.city shouldBe  businessLocation.physicalAddress.city
            mappedBusinessLocation.physicalAddress.province shouldBe  businessLocation.physicalAddress.province
            mappedBusinessLocation.physicalAddress.countryCode shouldBe  businessLocation.physicalAddress.country.countryCode
            mappedBusinessLocation.postalAddress.line1 shouldBe  businessLocation.postalAddress.line1
            mappedBusinessLocation.postalAddress.line2 shouldBe  businessLocation.postalAddress.line2
            mappedBusinessLocation.postalAddress.line3 shouldBe  businessLocation.postalAddress.line3
            mappedBusinessLocation.postalAddress.suburb shouldBe  businessLocation.postalAddress.suburb
            mappedBusinessLocation.postalAddress.city shouldBe  businessLocation.postalAddress.city
            mappedBusinessLocation.postalAddress.province shouldBe  businessLocation.postalAddress.province
            mappedBusinessLocation.postalAddress.countryCode shouldBe  businessLocation.postalAddress.country.countryCode

        }

        "should map multiple business location" {
            var mappedLocations = BusinessLocationMapper.mapFrom(listOf(BusinessLocationEntityBuilder.randomBusinessLocation(), BusinessLocationEntityBuilder.randomBusinessLocation())).size
            mappedLocations shouldBe 2
        }
    }
}