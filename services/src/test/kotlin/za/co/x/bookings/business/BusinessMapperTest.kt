package za.co.x.bookings.business


import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.junit.experimental.categories.Category
import za.co.x.bookings.UnitTest
import za.co.x.bookings.data.business.BusinessEntityBuilder
import za.co.x.bookings.repositories.ref.MyCategoryRepository
import za.co.x.bookings.repositories.ref.MyCountryRepository

@Category(UnitTest::class)
class BusinessMapperTest : StringSpec() {
    init {
        "should map business to entity"{
            val business = BusinessBuilder.randomBusiness()
            val mappedBusiness = BusinessMapper.mapFrom(business, MyCategoryRepository(), MyCountryRepository())

            mappedBusiness.name shouldBe  business.name
            mappedBusiness.description shouldBe  business.description
            mappedBusiness.emailAddress shouldBe business.emailAddress
            mappedBusiness.phoneNumber.phoneNumber shouldBe  business.phoneNumber.phoneNumber
            mappedBusiness.phoneNumber.country.countryCode shouldBe  business.phoneNumber.countryCode
            mappedBusiness.physicalAddress.line1 shouldBe  business.physicalAddress.line1
            mappedBusiness.physicalAddress.line2 shouldBe  business.physicalAddress.line2
            mappedBusiness.physicalAddress.line3 shouldBe  business.physicalAddress.line3
            mappedBusiness.physicalAddress.suburb shouldBe  business.physicalAddress.suburb
            mappedBusiness.physicalAddress.city shouldBe  business.physicalAddress.city
            mappedBusiness.physicalAddress.province shouldBe  business.physicalAddress.province
            mappedBusiness.physicalAddress.country.countryCode shouldBe  business.physicalAddress.countryCode
        }

        "should map business from entity" {
            val businessEntity  = BusinessEntityBuilder.randomBusiness()
            val mappedBusiness = BusinessMapper.mapFrom(businessEntity)

            mappedBusiness.name shouldBe  businessEntity.name
            mappedBusiness.description shouldBe  businessEntity.description
            mappedBusiness.emailAddress shouldBe businessEntity.emailAddress
            mappedBusiness.phoneNumber.phoneNumber shouldBe  businessEntity.phoneNumber.phoneNumber
            mappedBusiness.phoneNumber.countryCode shouldBe  businessEntity.phoneNumber.country.countryCode
            mappedBusiness.physicalAddress.line1 shouldBe  businessEntity.physicalAddress.line1
            mappedBusiness.physicalAddress.line2 shouldBe  businessEntity.physicalAddress.line2
            mappedBusiness.physicalAddress.line3 shouldBe  businessEntity.physicalAddress.line3
            mappedBusiness.physicalAddress.suburb shouldBe  businessEntity.physicalAddress.suburb
            mappedBusiness.physicalAddress.city shouldBe  businessEntity.physicalAddress.city
            mappedBusiness.physicalAddress.province shouldBe  businessEntity.physicalAddress.province
            mappedBusiness.physicalAddress.countryCode shouldBe  businessEntity.physicalAddress.country.countryCode
        }
    }
}