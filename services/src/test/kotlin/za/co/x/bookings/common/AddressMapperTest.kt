package za.co.x.bookings.common


import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import org.junit.experimental.categories.Category
import za.co.x.bookings.UnitTest
import za.co.x.bookings.data.common.AddressEntityBuilder
import za.co.x.bookings.repositories.ref.MyCountryRepository
@Category(UnitTest::class)
class AddressMapperTest : StringSpec(){
    init {
        "should map address data"{
            val addressData = AddressEntityBuilder.randomAddress()
            val mappedAddress = AddressMapper.mapFrom(addressData)

            mappedAddress.line1 shouldBe  addressData.line1
            mappedAddress.line2 shouldBe  addressData.line2
            mappedAddress.line3 shouldBe  addressData.line3
            mappedAddress.city shouldBe  addressData.city
            mappedAddress.suburb shouldBe  addressData.suburb
            mappedAddress.province shouldBe addressData.province
            mappedAddress.countryCode shouldBe addressData.country.countryCode

        }

        "should map multiple addresses"{
            val mappedAddresses = AddressMapper.mapFrom(listOf(AddressEntityBuilder.randomAddress(), AddressEntityBuilder.randomAddress(), AddressEntityBuilder.randomAddress()))
            mappedAddresses.size shouldBe  3
        }

        "should map address to entity" {

            val address = AddressBuilder.randomAddress()
            val mappedAddressEntity = AddressMapper.mapFrom(address, MyCountryRepository())

            mappedAddressEntity.line1 shouldBe  address.line1
            mappedAddressEntity.line2 shouldBe  address.line2
            mappedAddressEntity.line3 shouldBe  address.line3
            mappedAddressEntity.city shouldBe  address.city
            mappedAddressEntity.suburb shouldBe  address.suburb
            mappedAddressEntity.province shouldBe address.province
            mappedAddressEntity.country.countryCode shouldBe address.countryCode
        }

        "should map multiple address entities"{
            val mappedAddresses = AddressMapper.mapFrom(listOf(AddressBuilder.randomAddress(),AddressBuilder.randomAddress(),AddressBuilder.randomAddress()), MyCountryRepository())
            mappedAddresses.size shouldBe  3
        }
    }
}

