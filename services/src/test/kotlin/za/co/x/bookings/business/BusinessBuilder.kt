package za.co.x.bookings.business

import com.github.javafaker.Faker
import za.co.x.bookings.business.BusinessLocationBuilder.Companion.randomBusinessLocation
import za.co.x.bookings.common.AddressBuilder
import za.co.x.bookings.common.PhoneNumberBuilder
import za.co.x.bookings.data.business.BusinessLocationEntityBuilder
import za.co.x.bookings.data.common.AddressEntityBuilder
import za.co.x.bookings.data.common.PhoneNumberEntityBuilder
import za.co.x.bookings.data.ref.CategoryEntityBuilder
import java.util.*

class BusinessBuilder {
    companion object {
        private val faker = Faker()
        fun randomBusiness() = Business(
                UUID.randomUUID(),
                faker.random().nextInt(100),
                faker.name().name(),
                faker.lorem().paragraph(),
                faker.internet().emailAddress(),
                PhoneNumberBuilder.randomPhoneNumber(),
                AddressBuilder.randomAddress(),
                AddressBuilder.randomAddress(),
                listOf(randomBusinessLocation(),randomBusinessLocation())
        )

        fun randomBusinessEntity() = za.co.x.bookings.data.business.Business(
                faker.random().nextLong(),
                UUID.randomUUID(),
                CategoryEntityBuilder.randomCategory(),
                faker.company().name(),
                faker.company().catchPhrase(),
                faker.internet().emailAddress(),
                PhoneNumberEntityBuilder.randomPhoneNumber(),
                AddressEntityBuilder.randomAddress(),
                AddressEntityBuilder.randomAddress(),
                locations = mutableSetOf(BusinessLocationEntityBuilder.randomBusinessLocation())

        )
    }
}