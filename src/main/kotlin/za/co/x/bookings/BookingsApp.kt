package za.co.x.bookings

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import za.co.x.bookings.config.ServicesConfiguration


@SpringBootApplication
@Configuration
@Import(ServicesConfiguration::class)
open class BookingsApp {
}

fun main(args: Array<String>) {
    SpringApplication.run(BookingsApp::class.java, *args)
}